import VueRouter from 'vue-router'
import StartPage from './components/StartPage/StartPage'
import Game from './components/Game/Game'

const routes = [
    {
        path: '/',
        component: StartPage,
    },
    {
        path: '/play/:playerName',
        component: Game,
    }
]

const router = new VueRouter({routes})

export default router;