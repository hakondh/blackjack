export function shuffleDeck() {
    return fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
        .then((response => response.json()))
}

export function getCards(deckId, amount) {
   return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${amount}`)
       .then((response => response.json()))
}